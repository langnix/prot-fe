# prot-fe

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).


## Auth0
-> MetadataService.getMetadataProperty: Metadata does not contain optional property end_session_endpoint
Daher klappt der logout anscheinend nicht (so) richtig.

TODO: gibt es eine "Signoff-URL" bei auth0?
 * JA: https://auth0.com/docs/api/authentication#logout
```
GET /v2/logout 
returnTo	URL to redirect the user after the logout.
client_id	The client_id of your application.
federated	Add this query string parameter to the logout URL, to log the user out of their identity provider, as well: https://YOUR_DOMAIN/v2/logout?federated.
```
TODO:  wie konfiguriert man die, so dass der Logout genutzt wird  
  
