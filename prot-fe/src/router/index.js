import {createRouter, createWebHistory} from 'vue-router'
import Home from '../views/Home.vue'
import About from '../views/About.vue'
import Manage from '../views/Manage.vue'
import idsrvAuth from '../idsrvAuth'
import Results from "@/views/Results";
import Division from "@/views/Division";
import Match from "@/views/Match";

const routes = [
    {
        path: '/',
        name: 'Home',
        component: Home
    },
    {
        path: '/about',
        name: 'About',
        component: About
    },
    {
        path: '/manage',
        component: Manage
    },
    {
        path: '/manage/results',
        component: Results,
    },
    {
        path: '/manage/results/team/:teamId',
        component: Division,props: true
    },

    {
        path: '/manage/results/match/:matchId',
        component: Match,props: true
    },
].map(rr => {
        if (rr.path.startsWith("/manage")) {
            rr.meta = {
                authName: idsrvAuth.authName
            }
        }
        return rr
    })

const router = createRouter({
    history: createWebHistory(process.env.BASE_URL),
    routes
})

idsrvAuth.useRouter(router)

export default router
